<?php

function connect()
{
    $servername = "localhost";
    $username = "josemar";
    $password = "1234";
    $dbname = "booking";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        return "Connection failed: " . $e->getMessage();
    }
}

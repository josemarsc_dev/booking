<?php

header('Content-Type: text/html; charset=utf-8');
header('Access-Control-Allow-Origin: http://127.0.0.1');
header('Access-Control-Allow-Methods: POST, GET, DELETE');

include "db/Operations.php";

$ret = 'nada foi passado';

$table = null;
$op = null;
$params = null;
$id = null;

if (isset($_REQUEST['table'])) {
    $table = $_REQUEST['table'];
    if (isset($_REQUEST['op'])) {
        $op = $_REQUEST['op'];

        if (isset($_REQUEST['params'])) {
            $params = $_REQUEST['params'];
        }

        switch ($op) {
            case 'consulta_viagem':
                $rows = null;
                $conditions = null;
                if (isset($params['rows'])) {
                    $rows = $params['rows'];
                }
                if (isset($params['conditions'])) {
                    $conditions = $params['conditions'];
                }
                $ret = consulta_viagem($conditions);
                break;
            case 'select':
                $rows = null;
                $conditions = null;
                if (isset($params['rows'])) {
                    $rows = $params['rows'];
                }
                if (isset($params['conditions'])) {
                    $conditions = $params['conditions'];
                    // $ret = $conditions;
                    // break;
                }
                $ret = select($table, $rows, $conditions);
                break;
            case 'insert':
                if (isset($params['insertData'])) {
                    $insertData = $params['insertData'];
                    $ret = insert($table, $insertData);
                } else {
                    $ret = "parametro insertData não foi passado";
                }
                break;
            case 'delete':
                if (isset($_REQUEST['id'])) {
                    $id = $_REQUEST['id'];
                    $ret = delete($table, $id);
                } else {
                    $ret = "parametro id não foi passado";
                }
                break;
            case 'update':
                if (isset($_REQUEST['id'])) {
                    $id = $_REQUEST['id'];
                    if (isset($params['updateData'])) {
                        $updateData = $params['updateData'];
                        $ret = update($table, $id, $updateData);
                    } else {
                        $ret = "parametro updateData não foi passado";
                    }
                } else {
                    $ret = "parametro id não foi passado";
                }
                break;
            default:
                $ret = "variavel op invalida";
                break;
        }

    } else {
        $ret = "parametro op não foi passado";
    }
} else {
    $ret = "parametro table não foi passado";
}

echo json_encode($ret);

$(document).ready(function () {
    $('#login-btn').on('click', (ev) => {
        window.location = "../login"
    });
});

function formSubmit() {
    let nome = $('#nome').val();
    let email = $('#email').val();
    let data_nascimento = $('#data-nascimento').val();
    let cpf = $('#cpf').val();
    let doc_rg = $('#doc-rg').val();
    let tel_res = $('#telefone-res').val();
    let tel_cel = $('#telefone-cel').val();
    let senha = $('#senha').val();
    let confirma_senha = $('#confirma-senha').val();

    $.ajax({
        type: "POST",
        url: "http://localhost/eng_soft/booking/database/routes.php",
        data: {
            table: 'usuario',
            op: 'insert',
            params: {
                insertData: {
                    nome: nome,
                    email: email,
                    data_nascimento: data_nascimento,
                    cpf: cpf,
                    doc_rg: doc_rg,
                    telefone_cel: tel_cel,
                    telefone_res: tel_res,
                    senha: senha
                }
            }
        },
        success: function (response) {
            if(JSON.parse(response).length == 1) {
                window.location = "../consulta"
            } else {
                alert("Erro ao salvar");
            }
        }
    });
}
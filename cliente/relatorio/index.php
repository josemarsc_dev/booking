<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../frameworks/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="relatorio.css">
    <title>Relatorio de Reservas</title>
</head>


<body>
    <?php
        session_start();
        echo '<script>console.log(' . json_encode($_SESSION) . ');</script>';
    ?>

    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="../">
            <img src="../../img/logo.jpeg" width="30" height="30" alt="">
            <span>TravelTeO</span>
        </a>
        <div class="form-inline" id="usuario-info">
            <input class="form-control mr-sm-2 btn-secondary" type="button" id="login-btn" value="Login">
        </div>
    </nav>

    <div id="container">
        <h1>Relatorio de Reservas</h1>

        <div id="">

        </div>
        
    </div>
    <script src="../../frameworks/jquery.js"></script>
    <script src="relatorio.js"></script>
</body>

</html>
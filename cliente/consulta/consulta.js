$(document).ready(function () {
    $('#somente-ida').on('click', (ev) => {
        if (ev['currentTarget']['checked'] == true) {
            $('#data-volta').attr('disabled', 'disabled');
        } else {
            $('#data-volta').removeAttr('disabled');
        }
    });

    $('#registrar-btn').on('click', (ev) => {
        window.location = "../registrar/"
    });

    $('#login-btn').on('click', (ev) => {
        window.location = "../login/"
    });

    $('#pesquisar-btn').on('click', (ev) => {
        let data_ida = $('#data-ida').val();
        let data_volta = $('#data-volta').val();
        let origem = $('#cidade-origem').val();
        let destino = $('#cidade-destino').val();
        console.log('data-ida: ' + data_ida + '\ndata-volta: ' + data_volta + '\norigem: ' + origem + '\nDestino: ' + destino);

        if (data_ida == '') {
            // let warning = `
            //     <div style="position: fixed; bottom: 5px; right: 5px; background-color: pink; width: 300px; height: 80px;">Data de ida é Obrigatória</div>
            // `;
            $('body').append('<script>alert("Data de ida é obrigatório");</script>');
        } else if (origem == 0) {
            $('body').append('<script>alert("Cidade Origem é obrigatório");</script>');
        } else if (destino == 0) {
            $('body').append('<script>alert("Cidade Destino é obrigatório");</script>');
        } else {
            console.log(origem, destino);

            $.ajax({
                type: "POST",
                url: "http://localhost/eng_soft/booking/database/routes.php",
                data: {
                    table: 'viagem',
                    op: 'consulta_viagem',
                    params: {
                        conditions: {
                            "cidade_origem" : origem,
                            "cidade_destino" : destino,
                            "data_ida": data_ida,
                            "data_volta": data_volta
                        }
                    }
                },
                success: function (response) {
                    let viagens = JSON.parse(response);
                    console.log(viagens);
                }
            });

            // $.ajax({
            //     type: "POST",
            //     url: "http://localhost/eng_soft/booking/database/routes.php",
            //     data: {
            //         table: 'viagem, linha',
            //         op: 'select',
            //         params: {
            //             rows: [
            //                 "viagem.id",
            //                 "data",
            //                 "hora",
            //                 "fklinha as linha_id",
            //                 "fkveiculo as veiculo",
            //                 "cidade_origem",
            //                 "cidade_destino",
            //                 "preco_passagem"
            //             ],
            //             conditions: {
            //                 "viagem.fklinha =" : "linha.id",
            //                 "cidade_origem =" : origem,
            //                 "cidade_destino =" : destino
            //             }
            //         }
            //     },
            //     success: function (response) {
            //         let viagens = JSON.parse(response);
            //         console.log(viagens);
            //     }
            // });
        }
    });

    $.ajax({
        type: "POST",
        url: "http://localhost/eng_soft/booking/database/routes.php",
        data: {
            table: 'cidade',
            op: 'select'
        },
        success: function (response) {
            console.log(response);
            let cidades = JSON.parse(response);
            montaSelectCidadeOrigem(cidades);
        },
        error: (response) => {
            console.log(response);
        }

    });

    $('#cidade-origem').change(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "http://localhost/eng_soft/booking/database/routes.php",
            data: {
                table: 'linha',
                op: 'select',
                params: {
                    rows: [
                        "id",
                        "(SELECT nome FROM cidade WHERE id = cidade_destino) AS nome",
                        "cidade_destino"
                    ],
                    conditions: {
                        'cidade_origem = ': $('#cidade-origem').val()
                    }
                }
            },
            success: function (response) {
                let cidades = JSON.parse(response);
                montaSelectCidadeDestino(cidades);
            }
        });
    });
});

function montaSelectCidadeOrigem(cidades) {
    let options = '';
    for (let i = 0; i < cidades.length; i++) {
        options += '<option value="' + cidades[i]['id'] + '">' + cidades[i]['nome'] + '</option>';
    }
    $("#cidade-origem").append(options);
}

function montaSelectCidadeDestino(cidades) {
    let options = '';
    if (cidades.length == 0) {
        options += '<option value="0">Não há destinos para a cidade selecionada</option>';
    } else if (cidades.length > 1) {
        options += '<option value="0">Selecione...</option>';
    }
    for (let i = 0; i < cidades.length; i++) {
        options += '<option value="' + cidades[i]['cidade_destino'] + '">' + cidades[i]['nome'] + '</option>';
    }
    $("#cidade-destino").html(options);
}
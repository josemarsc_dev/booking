<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../frameworks/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="login.css">
    <title>Login</title>
</head>

<body>

    <?php
        session_start();
        echo '<script>console.log(' . json_encode($_SESSION) . ');</script>';
    ?>

    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="../">
            <img src="../../img/logo.jpeg" width="30" height="30" alt="">
            <span>TravelTeO</span>
        </a>
        <div class="form-inline" id="usuario-info">
            <input class="form-control mr-sm-2 btn-secondary" type="button" id="registrar-btn" value="Registrar">
        </div>
    </nav>

    <div id="form-login">
        <h1>Login</h1>
        <form action="#" onsubmit="login(); return false;">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="fulano@domain.com" required>
            </div>
            <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" class="form-control" id="senha" placeholder="********" autocomplete="off" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <script src="../../frameworks/jquery.js"></script>
    <script src="login.js"></script>
</body>

</html>
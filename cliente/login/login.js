$(document).ready(function () {
    $('#registrar-btn').on('click', (ev) => {
        ev.preventDefault();
        window.location = "../registrar"
    });
});

function login() {
    let email = $('#email').val();
    let senha = $('#senha').val();
    console.log(email, senha);
    $.ajax({
        type: "POST",
        url: "http://localhost/eng_soft/booking/database/routes.php",
        data: {
            table: 'usuario',
            op: 'select',
            params: {
                rows: [
                    "id", "nome", "email", "data_nascimento", "telefone_res", "telefone_cel", "doc_rg", "cpf"
                ],
                conditions: {
                    "email = ": email,
                    "senha = ": senha
                }
            }
        },
        success: function (response) {
            let user = JSON.parse(response);
            console.log(user);
            if(user.length == 0) {
                console.log("Usuário não encontrado!");
                alert("Usuário não encontrado!");
            } else {
                window.location = "../consulta";
            }
        }
    });
}
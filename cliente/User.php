<?php
class User {
    private $id;
    private $nome;
    private $email;
    private $data_nascimento;
    private $doc_rg;
    private $cpf;
    private $telefone_res;
    private $telefone_cel;
    
    public function __get($property) {
        return $this->$property;
    }

    public function __set($property, $value) {
        $this->$property = $value;
    }
}
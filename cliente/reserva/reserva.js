$(document).ready(function () {
    $('#somente-ida').on('click', (ev) => {
        if (ev['currentTarget']['checked'] == true) {
            $('#data-volta').attr('disabled', 'disabled');
        } else {
            $('#data-volta').removeAttr('disabled');
        }
    });

    $('#registrar-btn').on('click', (ev) => {
        window.location = "../registrar/"
    });

    $('#login-btn').on('click', (ev) => {
        window.location = "../login/"
    });

    $('#reserva-btn').on('click', (ev) => {
        let data_ida = $('#data-ida').val();
        let data_volta = $('#data-volta').val();
        let origem = $('#cidade-origem').val();
        let destino = $('#cidade-destino').val();
        console.log('data-ida: ' + data_ida + '\ndata-volta: ' + data_volta + '\norigem: ' + origem + '\nDestino: ' + destino);
    });

    $.ajax({
        type: "POST",
        url: "http://localhost/eng_soft/booking/database/routes.php",
        data: {
            table: 'cidade',
            op: 'select'
        },
        success: function (response) {
            let cidades = JSON.parse(response);
            montaSelectCidadeOrigem(cidades);
        }
    });

    $('#cidade-origem').change(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "http://localhost/eng_soft/booking/database/routes.php",
            data: {
                table: 'linha',
                op: 'select',
                params: {
                    rows: [
                        "id",
                        "(SELECT nome FROM cidade WHERE id = cidade_destino) AS nome"
                    ],
                    conditions: {
                        'cidade_origem = ': $('#cidade-origem').val()
                    }
                }
            },
            success: function (response) {
                let cidades = JSON.parse(response);
                montaSelectCidadeDestino(cidades);
            }
        });
    });
});

function montaSelectCidadeOrigem(cidades) {
    let options = '';
    for (let i = 0; i < cidades.length; i++) {
        options += '<option value="' + cidades[i]['id'] + '">' + cidades[i]['nome'] + '</option>';
    }
    $("#cidade-origem").append(options);
}

function montaSelectCidadeDestino(cidades) {
    let options = '';
    if(cidades.length == 0) {
        options += '<option value="0">Não há destinos para a cidade selecionada</option>';
    } else if(cidades.length > 1) {
        options += '<option value="0">Selecione...</option>';
    }
    for (let i = 0; i < cidades.length; i++) {
        options += '<option value="' + cidades[i]['id'] + '">' + cidades[i]['nome'] + '</option>';
    }
    $("#cidade-destino").html(options);
}
<!DOCTYPE html>
<html lang="br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../frameworks/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="reserva.css">
    <title>Efetuar Reserva</title>
</head>

<body>

    <?php
        session_start();
        echo '<script>console.log(' . json_encode($_SESSION) . ');</script>';
    ?>

    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="">
            <img src="../../img/logo.jpeg" width="30" height="30" alt="">
            <span>TravelTeO</span>
        </a>
        <div class="form-inline" id="usuario-info">
            <input class="form-control mr-sm-2 btn-outline-secondary" type="button" id="registrar-btn"
                value="Registrar">
            <input class="form-control mr-sm-2 btn-secondary" type="button" id="login-btn" value="Login">
        </div>
    </nav>
    <div id="form-reservar-vagas">
        <h1>Efetuar Reserva</h1>
        <form>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="data-ida">Data Ida</label>
                    <input type="date" class="form-control" id="data-ida">
                </div>
                <div class="form-group col-md-6">
                    <label for="data-volta">Data Volta</label>
                    <input type="date" class="form-control" id="data-volta">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" placeholder="Nome Completo..." autofocus required>
                </div>
                <div class="form-group col-md-5">
                    <label for="data-nasc1">Data Nascimento</label>
                    <input type="date" class="form-control" id="data-nasc1">
                </div>
                <div class="form-group col-md-2">
                    <button type="button" class="form-control btn btn-success">+</button>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="nome">Nome do Responsável</label>
                    <input type="text" class="form-control" id="nome" placeholder="Nome do Responsável" autofocus
                        required>
                </div>

                <div class="form-group">
                    <label for="poltronas">Quantidade de Poltronas</label>
                    <select id="poltronas" class="form-control">
                        <option selected>Quantidade de Poltronas</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="cidade-origem">Origem</label>
                    <select id="cidade-origem" class="form-control">
                        <option selected>Cidade Origem...</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="cidade-destino">Destino</label>
                    <select id="cidade-destino" class="form-control">
                        <option selected>Cidade Destino...</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="somente-ida">
                    <label class="form-check-label" for="somente-ida">
                        Somente ida
                    </label>
                </div>
            </div>
            <button type="button" class="btn btn-primary" id="confirmar-btn">Confirmar</button>
        </form>
    </div>
    </div>

    <script src="../../frameworks/jquery.js"></script>
    <script src="reserva.js"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../frameworks/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="registrar.css">
    <title>Consultar Vagas</title>
</head>

<body>
    <?php
        session_start();
        echo '<script>console.log(' . json_encode($_SESSION) . ');</script>';
    ?>

    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="../">
            <img src="../../img/logo.jpeg" width="30" height="30" alt="">
            <span>TravelTeO</span>
        </a>
        <div class="form-inline" id="usuario-info">
            <input class="form-control mr-sm-2 btn-secondary" type="button" id="login-btn" value="Login">
        </div>
    </nav>

    <div id="form-registrar">
        <h1>Cadastrar Usuário</h1>
        <form action="#" onsubmit="formSubmit(); return false;">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" placeholder="Nome Completo..." autofocus required>
            </div>

            <div class="form-group">
                <label for="email">e-mail</label>
                <input type="email" class="form-control" id="email" placeholder="fulano@domain.com" required>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="data-nascimento">Data de Nascimento</label>
                    <input type="date" class="form-control" id="data-nascimento" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="cpf">CPF</label>
                    <input id="cpf" class="form-control" type="text" maxlength="14" placeholder="123.456.789-00"
                        required>
                </div>
                <div class="form-group col-md-4">
                    <label for="doc-rg">Identidade</label>
                    <input type="text" class="form-control" id="doc-rg" placeholder="MG 1234567" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="telefone-res">Telefone Residencial</label>
                    <input type="tel" class="form-control" id="telefone-res" placeholder="(33) 3522-0000" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="telefone-cel">Telefone Celular</label>
                    <input type="text" class="form-control" id="telefone-cel" placeholder="(33) 9 8888-0000" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" id="senha" placeholder="********" autocomplete="off"
                        required>
                </div>
                <div class="form-group col-md-6">
                    <label for="confirma-senha">Confirmar Senha</label>
                    <input type="text" class="form-control" id="confirma-senha" placeholder="********"
                        autocomplete="off" required>
                </div>
            </div>
            <button type="submit" id="registrar-btn" class="btn btn-primary">Cadastrar</button>
        </form>
    </div>
    <script src="../../frameworks/jquery.js"></script>
    <script src="registrar.js"></script>
</body>

</html>
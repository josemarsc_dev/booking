INSERT INTO usuario VALUES(
    1,
    "Josemar Silva",
    "1996-08-11",
    "MG12123123",
    "12312312312",
    "3333333333",
    "33988888888",
    "josemarsc.dev@gmail.com",
    MD5("1234")
);

INSERT INTO passageiro VALUES
    (1, '12312312311', 'Fulano um', '1996-10-09', 1),
    (2, '12312312312', 'Fulano dois', '1970-07-12', 1),
    (3, '12312312313', 'Fulano tres', '2015-10-09', 1);

INSERT INTO cidade VALUES
    (1, 'Teófilo Otoni', 'MG'),
    (2, 'Belo Horizonte', 'MG'),
    (3, 'Vitória', 'ES'),
    (4, 'Vitória da Conquista', 'BA');

INSERT INTO linha VALUES
    (1, 1, 2, 120.0),
    (2, 1, 3, 140.0),
    (3, 1, 4, 160.0),

    (4, 2, 1, 120.0),
    (5, 3, 1, 140.0),
    (6, 4, 1, 160.0);

INSERT INTO veiculo VALUES
    (1, 'Ônibus 1', 42, 'ABC1234', 'VW bus'),
    (2, 'Ônibus 2', 46, 'ABC4321', 'VW bus'),
    (3, 'Ônibus 3', 38, 'ABC1324', 'VW bus');

DELIMITER ;;
CREATE TRIGGER `after_insert_viagem` AFTER INSERT ON `viagem` FOR EACH ROW
BEGIN
    DECLARE x INTEGER;
    DECLARE linha INTEGER;
    DECLARE id_viagem INTEGER;
    SET x = (SELECT quantidade_poltronas FROM veiculo WHERE id = NEW.fkveiculo);
    SET linha = NEW.fklinha;
    SET id_viagem = NEW.id;
    WHILE x > 0 DO
        INSERT INTO poltrona_viagem(numero, preco_passagem, fkviagem) VALUES (x, (SELECT preco_passagem FROM linha WHERE id = linha), id_viagem);
        SET x = x - 1;
    END WHILE;
END;;
DELIMITER ;

INSERT INTO viagem VALUES
    (1, '2019-05-20', '21:00:00', 1, 3),
    (2, '2019-05-20', '21:00:00', 2, 2),
    (3, '2019-05-20', '21:00:00', 3, 1);
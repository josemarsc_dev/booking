CREATE DATABASE booking;

USE booking;

CREATE TABLE usuario(
    id INTEGER AUTO_INCREMENT,
    nome TEXT NOT NULL,
    data_nascimento DATE NOT NULL,
    doc_rg VARCHAR(20) NOT NULL UNIQUE,
    cpf CHAR(11) NOT NULL UNIQUE,
    telefone_res CHAR(10) NOT NULL,
    telefone_cel CHAR(11) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    senha CHAR(32) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE passageiro(
    id INTEGER AUTO_INCREMENT,
    cpf CHAR(11) NOT NULL UNIQUE,
    nome TEXT NOT NULL,
    data_nascimento DATE NOT NULL,
    usuario_responsavel INT NOT NULL,
    FOREIGN KEY(usuario_responsavel) REFERENCES usuario(id),
    PRIMARY KEY(id)
);

CREATE TABLE cidade(
    id INTEGER AUTO_INCREMENT,
    nome TEXT NOT NULL,
    estado CHAR(2) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE linha(
    id INTEGER AUTO_INCREMENT,
    cidade_origem INTEGER NOT NULL,
    cidade_destino INTEGER NOT NULL,
    preco_passagem DOUBLE,
    PRIMARY KEY(id),
    FOREIGN KEY(cidade_origem)REFERENCES cidade(id),
    FOREIGN KEY(cidade_destino)REFERENCES cidade(id)
);

CREATE TABLE veiculo(
    id INTEGER AUTO_INCREMENT,
    descricao TEXT NOT NULL,
    quantidade_poltronas INTEGER NOT NULL,
    placa CHAR(7),
    marca_modelo TEXT,
    PRIMARY KEY(id)
);

CREATE TABLE viagem(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    data DATE NOT NULL,
    hora TIME NOT NULL,
    fklinha INTEGER NOT NULL,
    fkveiculo INTEGER NOT NULL,
    FOREIGN KEY(fklinha) REFERENCES linha(id),
    FOREIGN KEY(fkveiculo) REFERENCES veiculo(id)
);

CREATE TABLE poltrona_viagem(
    numero INTEGER NOT NULL,
    preco_passagem DOUBLE NOT NULL,
    fkviagem INTEGER NOT NULL,
    fkpassageiro INTEGER NULL,
    fkusuario INTEGER NULL,
    FOREIGN KEY(fkviagem) REFERENCES viagem(id),
    FOREIGN KEY(fkpassageiro) REFERENCES passageiro(id),
    FOREIGN KEY(fkusuario) REFERENCES usuario(id)
);
SELECT
    usuario.id AS id_user, usuario.nome AS nome_user,
    linha.id AS id_linha, linha.cidade_origem, linha.cidade_destino,
    viagem.id AS id_viagem, viagem.data, viagem.hora,
    poltrona_viagem.numero AS poltrona, poltrona_viagem.preco_passagem, poltrona_viagem.nome_passageiro AS passageiro, poltrona_viagem.data_nascimento_passageiro
FROM 
    usuario, linha, viagem, poltrona_viagem
WHERE
    usuario.id = poltrona_viagem.fkusuario AND poltrona_viagem.fkviagem = viagem.id AND viagem.fklinha = linha.id;


SELECT
    id,
    (SELECT nome FROM cidade WHERE id = cidade_origem) AS cidade_origem,
    (SELECT nome FROM cidade WHERE id = cidade_destino) AS cidade_destino,
    preco_passagem
FROM
    linha
WHERE
    cidade_origem = 1;

SELECT
    id,
    (SELECT nome FROM cidade WHERE id = cidade_destino) AS nome
FROM
    linha
WHERE
    cidade_origem = 1;


SELECT
    viagem.id,
    data,
    hora,
    (SELECT id FROM cidade WHERE id = 1) AS id_cidade_origem,
    (SELECT nome FROM cidade WHERE id = 1) AS cidade_origem,
    (SELECT id FROM cidade WHERE id = 2) AS id_cidade_destino,
    (SELECT nome FROM cidade WHERE id = 2) AS cidade_destino
FROM 
    viagem, cidade, linha
WHERE
    viagem.fklinha = linha.id AND
    data = '2019-05-20' AND 
    linha.cidade_origem = 1 AND
    linha.cidade_destino = 2
GROUP BY
    viagem.id;
    
    
select 
    viagem.id as id_viagem,
    data, 
    hora,
    fklinha as linha_id,
    fkveiculo as veiculo,
    cidade_origem,
    cidade_destino,
    preco_passagem
from
    viagem, linha
where
    viagem.fklinha = linha.id and
    cidade_origem = 1
    and cidade_destino =2;